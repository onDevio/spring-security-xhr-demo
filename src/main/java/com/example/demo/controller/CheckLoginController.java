package com.example.demo.controller;

import java.util.Collection;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CheckLoginController {
	
	@GetMapping("/checklogin")
	public ResponseEntity<Object> checkLogin(HttpSession session, Authentication authentication) {
		String image = "/login-error.png";
		if (isUserLoggedIn()) {
			image = "/login-ok.png";
		}
		return ResponseEntity
				.status(HttpStatus.FOUND)
				.header("Location", image)
				.build();
	}

	private boolean isUserLoggedIn() {
		Authentication a = SecurityContextHolder.getContext().getAuthentication();
		if (a == null || !a.isAuthenticated()) {
			return false;
		}
		Collection<? extends GrantedAuthority> authorities = a.getAuthorities();
		if (authorities == null) {
			return false;
		}
		boolean isAnonymous = authorities.stream().anyMatch(x -> "ROLE_ANONYMOUS".equals(x.getAuthority()));
		return !isAnonymous;
	}

}
