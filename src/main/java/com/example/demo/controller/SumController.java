package com.example.demo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
@RestController
public class SumController {

	@GetMapping("/sum")
	public ResponseEntity<Integer> sum(@RequestParam("a") Integer a, @RequestParam("b") Integer b, Authentication authentication) {
		log.info(authentication.getName());
		Integer c = a + b;
		return ResponseEntity.ok(c); 
	}

}
