package com.example.demo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.Data;

@RestController
public class LoginController {
	
	@Data
	public static class User {
		private String user;
	}
	
	@PostMapping("/login")
	public ResponseEntity<User> login(Authentication authentication) {
		User user = new User();
		user.setUser(authentication.getName());
		return ResponseEntity.ok(user); 
	}

}
