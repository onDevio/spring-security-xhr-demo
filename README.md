# spring-security-xhr-demo

Demo Frontend/Backend de Login con POST XHR (ej. Angular) contra Spring Boot / Security con HTTP Basic Auth usando Cookie estándar para mantener sesión de futuras llamadas XHR, con soporte CORS y puertos distintos para frontend/backend, todo en uno.

# Pruebas

## Login frontend/backend en mismo puerto 8181


1. En Eclipse, lanzar SpringSecurityXHRDemoPort8181
3. Ir a http://localhost:8181
4. En este momento el usuario no está logueado
5. Darle al botón "Login"
6. Ahora ya está logueado
7. Ir a http://localhost:8181/public.html
8. Hacer click en el botón "Sum"
9. Hacer click Logout

## Login frontend puerto 9191, backend puerto 8181

1. En Eclipse, lanzar SpringSecurityXHRDemoPort8181
1. También lanzar SpringSecurityXHRDemoPort9191
3. Ir a http://localhost:9191
4. En este momento el usuario no está logueado
5. Darle al botón "Login"
6. Ahora ya está logueado
7. Ir a http://localhost:9191/public.html
8. Hacer click en el botón "Sum"
9. Hacer click Logout